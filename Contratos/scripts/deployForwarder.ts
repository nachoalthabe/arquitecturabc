import { ethers } from "hardhat";

async function main() {
  // Obtenemos una instancia del contrato
  const Forwarder = await ethers.getContractFactory("Forwarder");
  // Disparamos una transaccion de para deployar una nueva instancia
  const forwarder = await Forwarder.deploy("Arq.BC", "2");
  // Esperamos a que se confirme el deploy
  await forwarder.deployed()

  console.log("Forwarder deployado en ", forwarder.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
