import { ethers } from "hardhat";

async function main() {
  const Faucet = await ethers.getContractFactory("Faucet_ArqBC");
  const faucet = await Faucet.deploy(
    process.env.TOKEN,
    process.env.FORWARDER
  );
  await faucet.deployed();

  console.log("Faucet deployado en ", faucet.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
