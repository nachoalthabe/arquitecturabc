import { expect } from "chai";
import { ethers } from "hardhat";
import * as Contracts from "../typechain";

interface EIP712Domain {
  name: string;
  version: string;
  chainId: number;
  verifyingContract: string;
}

interface ForwardRequest {
  from: string;
  to: string;
  value: number;
  gas: number;
  nonce: number;
  data: string;
}

async function singMetatx(
  fromAddress: string,
  domain: EIP712Domain,
  message: ForwardRequest
) {
  const compose = {
    types: {
      EIP712Domain: [
        { name: "name", type: "string" },
        { name: "version", type: "string" },
        { name: "chainId", type: "uint256" },
        { name: "verifyingContract", type: "address" },
      ],
      ForwardRequest: [
        { name: "from", type: "address" },
        { name: "to", type: "address" },
        { name: "value", type: "uint256" },
        { name: "gas", type: "uint256" },
        { name: "nonce", type: "uint256" },
        { name: "data", type: "bytes" },
      ],
    },
    domain,
    primaryType: "ForwardRequest",
    message,
  };
  return ethers.provider.send("eth_signTypedData_v4", [fromAddress, compose]);
}

describe("ERC20_ArqBC", function () {
  let forwarder: Contracts.Forwarder;
  let token: Contracts.ERC20ArqBC;
  let faucet: Contracts.FaucetArqBC;
  before(async () => {
    const Forwarder = await ethers.getContractFactory("Forwarder");
    const Faucet = await ethers.getContractFactory("Faucet_ArqBC");
    const Token = await ethers.getContractFactory("ERC20_ArqBC");
    forwarder = await Forwarder.deploy("name", "version");
    token = await Token.deploy(forwarder.address) as Contracts.ERC20ArqBC;
    faucet = await Faucet.deploy(token.address, forwarder.address) as Contracts.FaucetArqBC;
    const mintRole = await token.MINTER_ROLE()
    await token.grantRole(mintRole, faucet.address)
    await faucet.deployed();
  });
  it("Token supply 0", async function () {
    expect(await token.totalSupply()).to.equal(0);
  });
  it("MetaMint", async () => {
    const data = faucet.interface.encodeFunctionData("dameToken");
    const user = (await ethers.getSigners())[3];
    const network = await ethers.provider.getNetwork();
    const gas = await ethers.provider.estimateGas({
      from: user.address,
      to: faucet.address,
      data,
    });
    const domain: EIP712Domain = {
      chainId: network.chainId,
      name: "name",
      version: "version",
      verifyingContract: forwarder.address,
    };
    const message: ForwardRequest = {
      from: user.address,
      to: faucet.address,
      value: 0,
      nonce: 0,
      gas: gas.toNumber(),
      data,
    };
    const signature = await singMetatx(user.address, domain, message);
    await expect(() => forwarder.execute(message, signature))
      .to.changeTokenBalance(token, user, ethers.utils.parseUnits('1', 18));
  });
});
