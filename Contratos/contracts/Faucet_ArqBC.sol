// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

interface IERC20_ArqBC is IERC20 {
    function mint(address to, uint256 amount) external;
}

contract Faucet_ArqBC is ERC2771Context, Ownable {
    IERC20_ArqBC public token;

    address public forwarder;

    constructor(IERC20_ArqBC token_, address forwarder_) ERC2771Context(forwarder_) Ownable() {
        token = token_;
        forwarder = forwarder_;
    }

    function dameToken() external {
        require(token.balanceOf(_msgSender()) == 0, "Tiene saldo");
        // https://docs.soliditylang.org/en/latest/units-and-global-variables.html?highlight=units#ether-units
        token.mint(_msgSender(), 1 ether);
    }

    function updateForwarder(address forwarder_) external onlyOwner {
        forwarder = forwarder_;
    }

    function isTrustedForwarder(address forwarder_) public view override returns (bool) {
        return forwarder_ == forwarder;
    }

    function _msgSender() internal view virtual override(Context, ERC2771Context) returns (address sender){
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view virtual override(Context, ERC2771Context) returns (bytes calldata){
        return ERC2771Context._msgData();
    }
}
