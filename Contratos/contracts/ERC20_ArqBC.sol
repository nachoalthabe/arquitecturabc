// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";

contract ERC20_ArqBC is ERC20, ERC20Burnable, AccessControl, ERC2771Context {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    address public forwarder;

    constructor(address forwarder_) ERC20("Arq.BC", "ABC") ERC2771Context(forwarder_) {
        forwarder = forwarder_;
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, msg.sender);
    }

    function mint(address to, uint256 amount) public onlyRole(MINTER_ROLE) {
        _mint(to, amount);
    }

    function updateForwarder(address forwarder_) external onlyRole(DEFAULT_ADMIN_ROLE) {
        forwarder = forwarder_;
    }

    function isTrustedForwarder(address forwarder_) public view override returns (bool) {
        return forwarder_ == forwarder;
    }

    function _msgSender() internal view virtual override(Context, ERC2771Context) returns (address sender){
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view virtual override(Context, ERC2771Context) returns (bytes calldata){
        return ERC2771Context._msgData();
    }
}
