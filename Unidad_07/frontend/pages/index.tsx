import {useEffect, useState} from "react";
import {ethers} from "ethers";
import * as Contracts from "../typechain"
import {JsonRpcSigner, Web3Provider} from "@ethersproject/providers";

export default function Home() {
    const [provider, setProvider] = useState<Web3Provider|null>(null)
    const [signer, setSigner] = useState<JsonRpcSigner|null>(null)
    const [address, setAddress] = useState<string|null>(null)

    useEffect(async () => {
        if (window && window['ethereum']){
            const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
            setProvider(provider)
            await provider.send("eth_requestAccounts", []);
            const signer = provider.getSigner()
            setSigner(signer)
            const address = await signer.getAddress()
            setAddress(address)
        }
    }, [])

    const [signature, setSignature] = useState<{signature: string, timestamp: number}|null>(null)
    const [transaction, setTransaction] = useState<ethers.ContractTransaction|null>(null)
    const [receipt, setReceipt] = useState<ethers.ContractReceipt|null>(null)

    async function sign(){
        if (!signer) return
        const timestamp =Math.round(Date.now() / 1000)
        setSignature({
            timestamp: timestamp,
            signature: await signer.signMessage(`Firma\nUnidad_07\n${timestamp}`)
        })
    }

    async function mint(){
        if (!signer) return
        console.log({
            forwarder: process.env.NEXT_PUBLIC_FAUCET,
            signer: signer
        })
        const transaction_ = await Contracts.FaucetArqBC__factory.connect(process.env.NEXT_PUBLIC_FAUCET, signer).dameToken()
        setTransaction(transaction_)
        setReceipt(await transaction_.wait(10))
    }

    return (
       <div>
           {provider === null ? (
               <h1>No conectado</h1>
            ):(
            <div>
                <h1>Address: {address}</h1>
                <div>
                    <button onClick={sign}>Firmar</button>
                    {signature && <div>
                        <p>timestamp:{ signature.timestamp }</p>
                        <p>Signature:{ signature.signature }</p>
                    </div>}
                </div>
                <div>
                    <button onClick={mint}>Mint</button>
                    {transaction && <div>
                        <p>tx hash:{ transaction.hash }</p>
                        <p>confirmations:{ transaction.confirmations }</p>
                        {receipt && <div>
                            <p>blocknumber:{ receipt.blockNumber }</p>
                        </div>}
                    </div>}

                </div>
            </div>

           )}
       </div>
    )
}
