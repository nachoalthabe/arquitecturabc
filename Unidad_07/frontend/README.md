1_ Instalar dependencias npm
2_ Si hay cambios en los contratos recordar actualizar las interfaces typechain
3_ Si se deploya un nuevo contrato, actualizar la dirección en .env
4_ Inicar servidor de local
```bash
    npm run dev
```
5_ Abrir el site http://localhost:3000/

Ejercicio:
1_ Agregar método para consultar balance de la cuenta conectada
2_ Agregar botón para quemar todo el balance de token que tiene la cuenta
