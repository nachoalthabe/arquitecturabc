import { Controller, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { Account, GetAccount, MetaMint, VerifySignature } from './DTOs';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('createWallet')
  @ApiCreatedResponse({
    type: Account,
  })
  async createWallet(@Body() params: GetAccount): Promise<Account> {
    return this.appService.createWallet(params.index);
  }

  @Post('verifySignature')
  async verifySignature(@Body() params: VerifySignature): Promise<boolean> {
    const message = `Firma\nUnidad_07\n${params.timestamp}`;
    // TODO: Cambiar address a verificar, este es el mecanismo clasico para autenticar a un usuario usando una wallet
    // basicamente se lo hace firmar un mensaje claro que defina:
    // * Contexto: de que sitio se trata, que acción y los parámetros clave
    // * Timestamp: Para poder verificar que el mensaje se haya firmado recientemente.
    // * Red: Se podría incluir la red en el mensaje, para dar certeza de que es producción o un entorno de pruebas.
    //   En este caso no es necesario
    // * Nonce: Se podría incluir un nonce para reducir la posibilidad de fraude.
    const myAddress = '0xF94d77AF2c48e06F189B3839bba9592D3699133A';
    return (
      this.appService.computeSigner(message, params.signature) === myAddress
    );
  }

  @Post('mint')
  async mint(@Body() params: MetaMint): Promise<string> {
    return this.appService.metaMint(params.index);
  }
}
