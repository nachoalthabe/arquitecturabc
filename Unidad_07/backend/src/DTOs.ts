import { ApiProperty } from '@nestjs/swagger';

export class GetAccount {
  @ApiProperty({ maximum: 2147483647 })
  index: number;
}

export class VerifySignature {
  @ApiProperty()
  timestamp: number;
  @ApiProperty()
  signature: string;
}

export class Account {
  @ApiProperty()
  address: string;
  @ApiProperty()
  publicKey: string;
}

export class MetaMint {
  @ApiProperty()
  index: number;
}
