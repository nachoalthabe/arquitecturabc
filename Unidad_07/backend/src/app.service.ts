import { Injectable } from '@nestjs/common';
import { Account } from './DTOs';
import * as ethers from 'ethers';
// Hace falta que corramos un
// npx hardhat compile
// en la carpeta de Contratos para que funcione el link
import * as Contracts from './typechain';

const mnemonic =
  'hybrid echo bamboo save later phone absent enroll minimum survey silent depend';
const hdNode = ethers.utils.HDNode.fromMnemonic(mnemonic);
const path = "m/44'/60'/0'";
const xPriv = hdNode.derivePath(path);
const xPub = xPriv.neuter();

// Para este ejemplo vamos a usar una base de datos en memoria
const MemoryDB = {
  accountByIndex: {},
  indexByAddress: {},
};

export interface ForwardRequest {
  from: string;
  to: string;
  value: ethers.BigNumber | number;
  gas: ethers.BigNumber;
  nonce: ethers.BigNumber;
  data: string;
}

const ForwardRequestType = {
  ForwardRequest: [
    { name: 'from', type: 'address' },
    { name: 'to', type: 'address' },
    { name: 'value', type: 'uint256' },
    { name: 'gas', type: 'uint256' },
    { name: 'nonce', type: 'uint256' },
    { name: 'data', type: 'bytes' },
  ],
};

@Injectable()
export class AppService {
  provider: ethers.providers.JsonRpcProvider;
  forwarder: Contracts.Forwarder;
  faucet: Contracts.FaucetArqBC;
  constructor() {
    this.provider = new ethers.providers.JsonRpcProvider(
      process.env.RINKEBY_URL,
    );
    this.forwarder = Contracts.Forwarder__factory.connect(
      process.env.FORWARDER,
      this.provider,
    );
    this.faucet = Contracts.FaucetArqBC__factory.connect(
      process.env.FAUCET,
      this.provider,
    );
  }
  createWallet(index: number): Account {
    if (!MemoryDB.accountByIndex[index]) {
      const pubKey = xPub.derivePath(index.toString());
      MemoryDB.accountByIndex[index] = {
        index,
        address: pubKey.address,
        publicKey: pubKey.publicKey,
      };
      MemoryDB.indexByAddress[pubKey.address] = index;
    }
    return MemoryDB.accountByIndex[index];
  }
  computeSigner(message: string, signature: string): string {
    return ethers.utils.verifyMessage(message, signature);
  }
  async getWalletNonce(address: string): Promise<ethers.BigNumber> {
    /**
     * Para evitar duplicación y asegurar la secuancialidad de las transacciones,
     * al igual que la red nativa, las meta-transaccione incorporan un nonce que
     * está en el contrato verificador, de esta forma podemos asegurara que hay
     * secuencialidad a nivel del forwarder.
     */
    const nonce = await this.forwarder.getNonce(address);
    return nonce;
  }
  async signMetaTransaction(
    index: number,
    message: ForwardRequest,
  ): Promise<string> {
    const priv = xPriv.derivePath(index.toString());
    const wallet = new ethers.Wallet(priv.privateKey);
    /**
     * El estandar EIP712 define la estructura general de
     * Dominio
     * Typo de mensaje
     * Mensaje
     * https://eips.ethereum.org/EIPS/eip-2771
     * En el caso de las meta transacciones EIP2771, el tipo de mensaje contiene
     * From: Cuenta del firmante
     * To: A donde esta dirigida la meta-transacción
     * Value: Cantidad de moneda nativa que vba a viajar en esta transaccion
     * Gas: El gas maximo que puede consumir la ejecución
     * Data: El llamado que se quiere ejecutar en to
     * Nonce: Indicador de secuencia de cada from
     */
    const signature = await wallet._signTypedData(
      {
        name: 'Arq.BC',
        chainId: 4,
        version: '2',
        verifyingContract: this.forwarder.address.toLocaleLowerCase(),
      },
      ForwardRequestType,
      message,
    );
    return signature;
  }
  async executeMetaTransaction(
    message: ForwardRequest,
    signature: string,
  ): Promise<string> {
    /**
     * Esta es la cuenta que va a pagar por ejecutar la transacción
     */
    const servicePk = process.env.PRIVATE_KEY as string;
    const signerPayer = new ethers.Wallet(servicePk, this.provider);
    const tx = await this.forwarder
      .connect(signerPayer)
      .execute(message, signature);
    await tx.wait();
    return tx.hash;
  }
  async metaMint(index: number): Promise<string> {
    const wallet = this.createWallet(index);
    const nonce = await this.getWalletNonce(wallet.address);
    /**
     * Este es el llamado a la función, en caso de que tenga parametros
     * van como vector seguido del nombre del metodo
     * await encodeFunctionData('methodA', ['Param1', 2, '0x03'])
     */
    const data = await this.faucet.interface.encodeFunctionData('dameToken');
    /**
     * Calculamos gas, eventualmente estima de menos y tenemos que sumar unos puntos,
     * esto se debe a que la interpretación de los atributos de las meta-transacciones
     */
    const gas = await this.provider.estimateGas({
      from: wallet.address,
      to: this.faucet.address,
      data,
    });
    const message = {
      from: wallet.address,
      to: this.faucet.address,
      gas: gas.add(gas.div(2)),
      value: 0,
      data,
      nonce,
    };
    const signature = await this.signMetaTransaction(index, message);
    return await this.executeMetaTransaction(message, signature);
  }
}
