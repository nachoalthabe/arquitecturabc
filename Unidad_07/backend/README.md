 * Instalar dependencias npm
 * Completar archivo .env con llave privada y url RPC
 * Si se deploya un nuevo contrato, actualizar la dirección en .env
 * Levantar entorno local  
    ```bash
     npm run start:dev
     ```
 * Abrir Swagger http://localhost:3001/api/

Ejercicio:
1_ Agregar método para consultar balance de la cuenta por indice
2_ Agregar botón para quemar todo el balance de token que tiene una cuenta por índice
