import { expect } from "chai"
import {ethers, upgrades} from "hardhat"

import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers"
import { Contrato, ContratoV2 } from "../typechain"

describe("Actualización", function () {

	let proxyAddress: string
	let admin: SignerWithAddress
	let thirdparty: SignerWithAddress

	before(async function () {
		[admin, thirdparty] = await ethers.getSigners()
	});

	describe("Contrato", function () {
		let contrato: Contrato
		before(async function () {
			const Contrato = await ethers.getContractFactory("Contrato")
			contrato = (await upgrades.deployProxy(Contrato)) as Contrato
			proxyAddress = contrato.address
		});
		it("Increment", async function () {
			await contrato.increment()
			expect(await contrato.value()).to.equal(2)
		});
	});

	describe( "ContratoV2", () => {
		let contrato: ContratoV2
		before(async () => {
			const ContratoV2 = await ethers.getContractFactory("ContratoV2")
			contrato = (await upgrades.upgradeProxy(proxyAddress, ContratoV2)) as ContratoV2
		})
		it("Decrement", async () => {
			contrato.decrement()
			expect(await contrato.value()).to.equal(1)
		})
		it("Debería fallar por underflow", async () => {
			await contrato.decrement()
			await expect(
				contrato.decrement()
			).to.be.revertedWith("panic code 0x11")
		})
	})

	describe( "ContratoV3", () => {
		it("Cambiar mensaje de revert 'panic code 0x11' por 'Es cero'", async () => {
		})
		it("Agregar un nuevo método para poder setear value que solo pueda ser llamado por cuentas con el rol 'SETTER_ROLE'", async () => {
			// La cuenta admin tiene rol DEFAULT_ADMIN_ROLE ademas de MOVER_ROLE,
			// con este rol le puede dar cualquier rol a cualquier cuenta.
		})
	})
});

describe("Desacoplamiento", () => {
	describe("Implementar una policy con interface IPolicy", () =>{
		describe("Agregar método de mint", () => {
			it("Solo una cuenta con rol Admin puede mintear", () => {})
		})
		describe("Soportar de blacklist, implementar metodo setBlacklist", () => {
			it("Una cuenta admin debe poder agregar y quitar cuentas de la blacklist", () => {})
			it("Si un address esta en la lista de blacklist no debe poder transferir tokens", () => {})
			it("Si un address esta en la lista de blacklist no debe poder recibir tokens", () => {})
		})
		describe("Soportar de Operadores Nativos, implementar metodo setDefaultOperator", () => {
			it("Un operador nativo deberia poder transferir balance de cualquier cuenta", async () => {})
		})
	})
})