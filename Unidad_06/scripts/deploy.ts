import { ethers, upgrades } from "hardhat";

async function main() {
  const Contrato = await ethers.getContractFactory("Contrato");
  const proxy = await upgrades.deployProxy(Contrato);
  await proxy.deployed();
  console.log("Contrato", proxy.address);
}
  
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });