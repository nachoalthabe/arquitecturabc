import { ethers, upgrades } from "hardhat";


async function main() {
    const contratoV2 = await ethers.getContractFactory("ContratoV2");
    await upgrades.upgradeProxy("0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0", contratoV2);
    console.log("Upgraded");
}
  
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });