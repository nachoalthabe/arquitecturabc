// SPDX-License-Identifier: MIT
pragma solidity ^0.8.12;

import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract ContratoV2 is Initializable, AccessControlUpgradeable {
    bytes32 public constant MOVER_ROLE = keccak256("MOVER_ROLE");

    uint256 public value;

    function initialize() initializer public {
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(MOVER_ROLE, _msgSender());
        // Value no se puede se en la linea 10 porque este contrato va a ser implementación de un proxy,
        // la asignación se realiza en la inicialización y no en la construcción.
        value = 1;
    }

    function increment() public onlyRole(MOVER_ROLE){
        value += 1;
    }

    function decrement() public onlyRole(MOVER_ROLE){
        value -= 1;
    }
}