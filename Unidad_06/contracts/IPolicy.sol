//SPDX-License-Identifier: MIT
pragma solidity ^0.8.12;

interface IPolicy {
    function mint(
        address account,
        uint256 amount
    ) external;

    function setBlacklist(
        address account,
        bool    isBlacklisted
    ) external;

    function setDefaultOperator(
        address account,
        bool    isDefaultOperator
    ) external;

    function isTransferable(
        address token,
        address operator,
        address from,
        address to,
        uint256 amount
    ) external view returns (bool);

    function isOperable(
        address token,
        address operator,
        address tokenHolder
    ) external view returns (bool);
}
