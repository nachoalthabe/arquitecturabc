// SPDX-License-Identifier: MIT
pragma solidity ^0.8.12;

import "@openzeppelin/contracts/token/ERC777/ERC777.sol";
import "./IPolicy.sol";

contract Token is ERC777 {
    IPolicy public policy;

    constructor(address[] memory defaultOperators) ERC777("Token", "TKN", defaultOperators) { }

    modifier onlyPolicy(){
        require(_msgSender() == address(policy), "onlyPolicy");
        _;
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256 amount
    ) internal view override {
        require(
            policy.isTransferable(address(this), operator, from, to, amount),
            "No transferable"
        );
    }

    function isOperatorFor(address operator, address tokenHolder)
        public
        view
        virtual
        override
        returns (bool)
    {
        return
        policy.isOperable(address(this), operator, tokenHolder) ||
        ERC777.isOperatorFor(operator, tokenHolder);
    }

    function mint(address to, uint256 amount) external onlyPolicy {
        _mint(to, amount, "", "");
    }

}
