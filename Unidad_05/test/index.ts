import { expect } from "chai";
import { ethers } from "hardhat";

import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { GoElevateCoin__factory, GoElevateCoin } from "../typechain";

describe("Token contract", function () {

	let GoElevateCoin: GoElevateCoin;
	let owner: SignerWithAddress
	let addr1: SignerWithAddress
	let addr2: SignerWithAddress
	let addrs: SignerWithAddress[];

	beforeEach(async function () {

		[owner, addr1, addr2, ...addrs] = await ethers.getSigners();

		const GoElevateCoinFactory = (await ethers.getContractFactory(
			"GoElevateCoin", owner
		)) as GoElevateCoin__factory;
		const totalSupply = (10 ** 9).toString()
		GoElevateCoin = await GoElevateCoinFactory.deploy(10**9,'GoElevateCoin',16,'GEC');

	});

	describe("Deployment", function () {

		it("Should assign the total supply of tokens to the owner", async function () {
			const ownerBalance = await GoElevateCoin.balanceOf(owner.address);
			expect(await GoElevateCoin.totalSupply()).to.equal(ownerBalance);
		});
	});

	describe("Transactions", function () {
		it("Should transfer tokens between accounts", async function () {
			// Transfer 50 tokens from owner to addr1
			await GoElevateCoin.transfer(addr1.address, 50);
			const addr1Balance = await GoElevateCoin.balanceOf(addr1.address);
			expect(addr1Balance).to.equal(50);

			// Transfer 50 tokens from addr1 to addr2
			// We use .connect(signer) to send a transaction from another account
			await GoElevateCoin.connect(addr1).transfer(addr2.address, 50);
			const addr2Balance = await GoElevateCoin.balanceOf(addr2.address);
			expect(addr2Balance).to.equal(50);
		});

		it("Should fail if sender doesn’t have enough tokens", async function () {
			const initialOwnerBalance = await GoElevateCoin.balanceOf(owner.address);

			// Try to send 1 token from addr1 (0 tokens) to owner (1000 tokens).
			// `require` will evaluate false and revert the transaction.
			await expect(
				GoElevateCoin.connect(addr1).transfer(owner.address, 1)
			).to.be.revertedWith("ERC20: transfer amount exceeds balance");

			// Owner balance shouldn't have changed.
			expect(await GoElevateCoin.balanceOf(owner.address)).to.equal(
				initialOwnerBalance
			);
		});

		it("Should update balances after transfers", async function () {
			const initialOwnerBalance = await GoElevateCoin.balanceOf(owner.address);

			// Transfer 100 tokens from owner to addr1.
			await GoElevateCoin.transfer(addr1.address, 100);

			// Transfer another 50 tokens from owner to addr2.
			await GoElevateCoin.transfer(addr2.address, 50);

			// Check balances.
			const finalOwnerBalance = await GoElevateCoin.balanceOf(owner.address);
			expect(finalOwnerBalance).to.equal(initialOwnerBalance.sub(150));

			const addr1Balance = await GoElevateCoin.balanceOf(addr1.address);
			expect(addr1Balance).to.equal(100);

			const addr2Balance = await GoElevateCoin.balanceOf(addr2.address);
			expect(addr2Balance).to.equal(50);
		});


		it("Should update allowance for user", async function () {
			const amountToAllow = 100
			// llamar al metodo `aprove` conectandose con la wallet `addr1` y aprobar `amountToAllow` a la direccion de la wallet `addr2`
			// invocar al metodo `allowance` para verificar obtener el monto autorizado por addr1 a addr2
			// verificar que el monto obtenido en el paso anterior es igual `amountToAllow`
		});

	});
});