import { ethers } from "hardhat";

async function main() {

  const GECToken = await ethers.getContractFactory("GoElevateCoin");
  console.log('Deploying GECToken...');
  const token = await GECToken.deploy(10**9,'GoElevateCoin',16,'GEC');

  await token.deployed();
  console.log("GECToken deployed to:", token.address);
}
  
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });